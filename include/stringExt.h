/* This is an extended version of <string> in c++11.
 *
 * Copyright (C) 2016, InvoTronix - Alice Heather <AliceHeather64@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef STRINGEXT_H
#define STRINGEXT_H

#include <string>
#include <sstream>
#include <vector>

namespace std
{
	inline std::vector<std::string> stov(std::string input, char delim)
	{
		std::vector<std::string> tempData;
		std::string strPart;
		std::stringstream ss;

		ss << input;

		while (std::getline(ss, strPart, delim))
		{
			tempData.push_back(strPart);
		}

		return tempData;
	}
};

#endif //STRINGEXT_H