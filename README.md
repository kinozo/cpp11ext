# c++11 Extended #

This header contains extended functions for c++11 standard libraries.

## Features ##

### <string> ###

* **std::stov** - *This function will split a std::string into a std::vector*

## Usage: ##

Just add the header file ***cpp11Ext.h*** below all the other standard libraries included from std in c++11.

## Licence ##

**GPLv3 - [http://www.gnu.org/licenses/gpl-3.0.html](http://www.gnu.org/licenses/gpl-3.0.html)**